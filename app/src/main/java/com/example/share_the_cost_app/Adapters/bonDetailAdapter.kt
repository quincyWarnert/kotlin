package com.example.share_the_cost_app.Adapters

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.share_the_cost_app.ui.fragments.EventDetail_IOU
import com.example.share_the_cost_app.ui.fragments.bonScreenDetail

class bonDetailAdapter(manager:FragmentManager):FragmentPagerAdapter(manager,
    BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): Fragment {
        val bonDetail=bonScreenDetail()
        val eventdetailIou = EventDetail_IOU()
        position + 1
        val bundle = Bundle()


        getPageTitle(position)
        bonDetail.arguments = bundle
        var fragment: Fragment? = null
        when (position) {
            0 -> fragment = bonDetail
            1 -> fragment = eventdetailIou
        }
        return fragment!!
    }
    override fun getPageTitle(position: Int): CharSequence? {
        val pageNr = when {
            position == 0 -> "Details"
            position == 1 -> "I.O.U"

            else -> "error"
        }
        return pageNr
    }

    override fun getCount(): Int {
      return 2
    }
}