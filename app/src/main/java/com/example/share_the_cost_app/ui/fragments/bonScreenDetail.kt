package com.example.share_the_cost_app.ui.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.share_the_cost_app.Adapters.bonviewattendeeAdapter
import com.example.share_the_cost_app.Adapters.fragAdapter

import com.example.share_the_cost_app.R


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [bonScreenDetail.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [bonScreenDetail.newInstance] factory method to
 * create an instance of this fragment.
 */
class bonScreenDetail : Fragment() {

    private var viewd: View? = null
    private lateinit var textView: TextView
    private var list = ArrayList<String>()
    private lateinit var recyclerView: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (viewd == null) {
            // Inflate the layout for this fragment
            viewd = inflater.inflate(R.layout.bonscreeniou, container, false)
            recyclerView = viewd!!.findViewById(R.id.bonViewAttendee)
            // textView = viewd!!.findViewById(R.id.bonlocation)
            list.add("Random Attendee")
            list.add("Random Dude 2")
            recyclerView.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = bonviewattendeeAdapter(list)
            }

            println("I'm Here bon screen")

        }
        return viewd
    }

}
