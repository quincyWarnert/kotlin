package com.example.share_the_cost_app.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.example.share_the_cost_app.Adapters.EventAdapter
import com.example.share_the_cost_app.Adapters.EventEditRecAdapter
import com.example.share_the_cost_app.Adapters.MyViewPagerAdapter
import com.example.share_the_cost_app.R
import com.example.share_the_cost_app.model.Attendee
import kotlinx.android.synthetic.main.activity_event_over_view.*
import kotlinx.android.synthetic.main.activity_main.*

class EventEdit : BaseActivity(),BaseActivity.OnDeleteListener,BaseActivity.OnChangeListener {
    private var attendeeList: MutableList<Attendee> = ArrayList<Attendee>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val actionBar = supportActionBar


        val eventName = intent.getStringExtra(("eventName"))
        val eventId = intent.getStringExtra("eventId")
        actionBar!!.title = eventName

        val eventEditAdapter=EventEditRecAdapter(attendeeList)
        eventEditAdapter.setOnChangeListener(this)
        eventEditAdapter.setOnDeleteListener(this)
        editRecView.apply {
            layoutManager=LinearLayoutManager(this@EventEdit)
            adapter=eventEditAdapter
        }
        btn_addUser.setOnClickListener(){
            val intent = Intent(applicationContext, AddUser::class.java)
            startActivity(intent)
        }
    }

    override fun OnDelete(position: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun OnChange(position: Int, choice: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
