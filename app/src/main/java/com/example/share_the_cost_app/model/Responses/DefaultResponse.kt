package com.example.share_the_cost_app.model.Responses

data class DefaultResponse(val error: Boolean, val message: String)