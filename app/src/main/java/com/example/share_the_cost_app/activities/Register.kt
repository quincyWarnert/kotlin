package com.example.share_the_cost_app.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import retrofit2.Callback
import retrofit2.Call
import com.example.share_the_cost_app.R
import com.example.share_the_cost_app.api.RetrofitClient
import com.example.share_the_cost_app.model.Responses.DefaultResponse
import com.example.share_the_cost_app.storage.SharedPrefManager
import kotlinx.android.synthetic.main.activity_register.*
import retrofit2.Response


class register : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        tvlogin.setOnClickListener() {
            startActivity(Intent(this, Login::class.java))
        }
    }

    fun registerUser(view: View) {
        val name = enterName!!.text.toString().trim()
        val email = enterEmail!!.text.toString().trim()
        val password = enterPassword!!.text.toString().trim()

        if (email.isEmpty()) {
            enterEmail.error = "email required"
            enterEmail.requestFocus()
            return
        }
        if (name.isEmpty()) {
            enterName.error = "Name required"
            enterName.requestFocus()
            return
        }
        if (password.isEmpty()) {
            enterPassword.error = "password required"
            enterPassword.requestFocus()
            return
        }
        RetrofitClient.instance.createUser(name, email, password)
            .enqueue(object : Callback<DefaultResponse> {
                override fun onResponse(
                    call: Call<DefaultResponse>,
                    response: Response<DefaultResponse>
                ) {
                    val intent = Intent(applicationContext, Profile::class.java)
                    intent.flags =
                        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)

                }

                override fun onFailure(call: Call<DefaultResponse>, t: Throwable) {
                    Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
                }
            })
    }

    override fun onStart() {
        super.onStart()
        if (SharedPrefManager.getInstance(this).isLoggedIn) {
            val intent = Intent(applicationContext, Profile::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
    }

}




