package com.example.share_the_cost_app.ui.fragments

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import com.example.share_the_cost_app.R
import com.example.share_the_cost_app.ui.AttendeeViewModel

class AttendeeFragment : Fragment() {

    companion object {
        fun newInstance() =
            AttendeeFragment()
    }

    private lateinit var viewModel: AttendeeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.attendee_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AttendeeViewModel::class.java)



    }


    class MyAdapter(private val myDataset: ArrayList<String>): RecyclerView.Adapter<MyAdapter.MyViewHolder>(){
        class  MyViewHolder(val textView: TextView): RecyclerView.ViewHolder(textView)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val textView=LayoutInflater.from(parent.context).inflate(R.layout.event_row,parent,false) as TextView
            return MyViewHolder(
                textView
            )
        }

        override fun getItemCount(): Int {
            return  myDataset.size
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.textView.text=myDataset[position]
        }

    }

}
