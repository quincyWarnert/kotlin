package com.example.share_the_cost_app.ui.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.share_the_cost_app.Adapters.fragAdapter
import com.example.share_the_cost_app.R
import kotlinx.android.synthetic.main.activity_event_over_view.*
import kotlinx.android.synthetic.main.fragment_event_detail.*
import kotlinx.android.synthetic.main.fragment_event_detail_bon.*

/**
 * A simple [Fragment] subclass.
 */
class EventDetail : Fragment() {
    private lateinit var textView: TextView
    private lateinit var recyclerView: RecyclerView
    private var listOfPeople = ArrayList<String>()
    private  var viewd:View?=null
    private lateinit var btn:Button

    init {

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        if(viewd==null) {
            // Inflate the layout for this fragment
            viewd = inflater.inflate(R.layout.fragment_event_detail, container, false)
            recyclerView = viewd!!.findViewById(R.id.selected_attendees)
            recyclerView.layoutManager = LinearLayoutManager(context) as RecyclerView.LayoutManager?
            recyclerView.adapter = fragAdapter(listOfPeople)
            textView = viewd!!.findViewById(R.id.fragmentDisplay)

            textView.setText(arguments?.getString("message"))
            listOfPeople.add("Rashford")
            listOfPeople.add("Defoe")
            listOfPeople.add("Henry")
            listOfPeople.add("Van Persie")
            listOfPeople.add("Rooney")
            println("I'm Here in event detail  create")
            println("content of list people dummy data: $listOfPeople")



        }
        return viewd

    }


}
