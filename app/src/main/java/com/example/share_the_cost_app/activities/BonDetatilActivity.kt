package com.example.share_the_cost_app.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.example.share_the_cost_app.Adapters.MyViewPagerAdapter
import com.example.share_the_cost_app.Adapters.bonDetailAdapter
import com.example.share_the_cost_app.R
import kotlinx.android.synthetic.main.fragment_event_detail_bon.*

class BonDetatilActivity :BaseActivity() {
    private var viewPage: ViewPager? = null
    private var myAdapter: bonDetailAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bon_detatil)
        viewPage = findViewById(R.id.pager)
        myAdapter = bonDetailAdapter(
            supportFragmentManager
        )
        viewPage?.adapter = myAdapter
        val actionBar = supportActionBar
        val bonName = intent.getStringExtra(("bonName"))
        actionBar!!.title = bonName

    }


}
