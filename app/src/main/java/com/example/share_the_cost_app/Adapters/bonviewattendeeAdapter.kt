package com.example.share_the_cost_app.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.share_the_cost_app.R
import kotlinx.android.synthetic.main.bonattendeeview.view.*
import kotlinx.android.synthetic.main.eventattendee_row.view.*

class bonviewattendeeAdapter(private val bonAttendee: ArrayList<String>) :
    RecyclerView.Adapter<bonviewattendeeAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.bonattendeeview, parent, false)
      return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
      return bonAttendee.size
    }


    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView){

        val bonAttendeeList: TextView = itemView.bonscreenName
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
       val data=bonAttendee[position]
        data.get(position)
        holder.bonAttendeeList.text=data
    }
}