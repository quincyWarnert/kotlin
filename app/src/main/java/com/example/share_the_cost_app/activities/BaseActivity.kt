package com.example.share_the_cost_app.activities


import android.view.MenuInflater
import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.share_the_cost_app.storage.SharedPrefManager
import android.os.Handler
import android.view.View
import android.widget.TextView
import com.example.share_the_cost_app.R
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


open class BaseActivity : AppCompatActivity() {

    val retrofit = Retrofit.Builder()
        .baseUrl("http://192.168.28.1/ApiStc/public/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(com.example.share_the_cost_app.R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        //add new element to menu.xml in layouts to add new Menu Option
        //then add to "when" code block here and add functions below
        return when (item.itemId) {
            R.id.profile -> {
                profileView()
                true
            }
            R.id.settings -> {
                val toast =
                    Toast.makeText(
                        applicationContext,
                        "settings underconstruction",
                        Toast.LENGTH_SHORT
                    ).show()
                true
            }
            R.id.logOut -> {
                logOutUser()
                true
            }
            R.id.attendeesView -> {
                goToAttendee()
                true
            }
            R.id.EditEvent-> {

                true
            }
            R.id.DeleteEvent-> {

                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }
    fun goToAttendee(){
        val intent=Intent(applicationContext,AddUser::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }
    fun logOutUser() {
        SharedPrefManager.getInstance(this).clear()
        val intent = Intent(applicationContext, Login::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    fun profileView() {
        val intent = Intent(applicationContext, Profile::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    // Back button methodes
    private var doubleBackToExitPressedOnce = false
    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {

            super.onBackPressed()


        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()
        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
        finish()
    }

    interface OnNoteListener {
        fun onNoteClick(position: Int, checked: Boolean)

    }
    interface OnEventListener {

        fun onEventClick(position: Int, eventD:String)
    }

    interface OnLongClicktListener  {

        fun LongClick(position: Int)
    }
    interface OnDeleteListener{
        fun OnDelete(position: Int)
    }
    interface OnChangeListener {
        fun OnChange(position: Int, choice:Int)
    }
}