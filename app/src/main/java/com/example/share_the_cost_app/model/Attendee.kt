package com.example.share_the_cost_app.model

data class Attendee (val id:Int, val name:String, val choice:Int=1)