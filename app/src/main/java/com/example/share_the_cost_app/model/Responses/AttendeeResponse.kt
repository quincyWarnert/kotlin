package com.example.share_the_cost_app.model.Responses

import com.example.share_the_cost_app.model.Attendee

data class AttendeeResponse(val error: Boolean, val attendee: List<Attendee>)