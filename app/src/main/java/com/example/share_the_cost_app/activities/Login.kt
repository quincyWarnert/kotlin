package com.example.share_the_cost_app.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.share_the_cost_app.R
import com.example.share_the_cost_app.api.RetrofitClient
import com.example.share_the_cost_app.model.Responses.LoginResponse
import com.example.share_the_cost_app.storage.SharedPrefManager
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Login : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        txtRegistered.setOnClickListener() {
            startActivity(Intent(this, register::class.java))
        }
        btnLogin.setOnClickListener() {
            val email = loginEmail!!.text.toString().trim()
            val password = loginPassword!!.text.toString().trim()
            if (email.isEmpty()) {
                loginEmail.error = "email required"
                loginEmail.requestFocus()
                return@setOnClickListener
            }
            if (password.isEmpty()) {
                loginPassword.error = "password required"
                loginPassword.requestFocus()
                return@setOnClickListener
            }
            RetrofitClient.instance.userLogin(email, password)
                .enqueue(object : Callback<LoginResponse> {
                    override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                        Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
                    }

                    override fun onResponse(
                        call: Call<LoginResponse>, response: Response<LoginResponse>
                    ) {
                        if (!response.body()?.error!!) {
                            val prefs = SharedPrefManager.getInstance(applicationContext)
                                prefs.saveUser(response.body()?.user!!)
                            prefs.saveAllAttendees(emptyList())

                            val intent = Intent(applicationContext, Profile::class.java)
                            intent.putExtra("username",response.body()!!.user.name)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(intent)
                        } else {
                            Toast.makeText(
                                applicationContext,
                                response.body()?.message,
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                })
        }
    }

    override fun onStart() {
        super.onStart()
        if (SharedPrefManager.getInstance(this).isLoggedIn) {
            val intent = Intent(applicationContext, Profile::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
    }

}




