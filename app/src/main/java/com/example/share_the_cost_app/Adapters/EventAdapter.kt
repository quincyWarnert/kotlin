package com.example.share_the_cost_app.Adapters


import android.view.*
import android.view.ContextMenu.ContextMenuInfo
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.share_the_cost_app.R
import com.example.share_the_cost_app.activities.BaseActivity.OnEventListener
import com.example.share_the_cost_app.activities.BaseActivity.OnLongClicktListener
import com.example.share_the_cost_app.model.Event
import kotlinx.android.synthetic.main.event_row.view.*


class EventAdapter(private val events: List<Event>) :
    RecyclerView.Adapter<EventAdapter.ViewHolder>() {

    private var mOnEventListener: OnEventListener? = null
    private var longEventListener: OnLongClicktListener? = null

    fun setOnClickListener(listener: OnEventListener?) {
        mOnEventListener = listener
    }

    fun setOnLongClickListener(listener: OnLongClicktListener?) {

        longEventListener = listener
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.event_row, parent, false)
        val viewHolder =
            ViewHolder(view)
        viewHolder.setOnClickListener(mOnEventListener)
        viewHolder.setOnLongClickListener(longEventListener)

        return viewHolder

    }

    override fun getItemCount() = events.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val event = events[position]
        events.get(position)
        holder.eventrow.text = event.name


    }

    class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener, OnLongClicktListener,
        View.OnCreateContextMenuListener,
        View.OnLongClickListener {

        private var mOnEventListener: OnEventListener? = null
        private var longEventListener: OnLongClicktListener? = null

        val eventclicked = itemView.eventRow

        init {
            eventclicked.setOnClickListener(this)
            //eventclicked.setOnLongClickListener(this)
            eventclicked.setOnCreateContextMenuListener(this)


        }

        fun setOnClickListener(listener: OnEventListener?) {
            mOnEventListener = listener
        }

        fun setOnLongClickListener(listener: OnLongClicktListener?) {
            longEventListener = listener
        }


        override fun onClick(v: View?) {

            println("inside event onclick")
            mOnEventListener?.onEventClick(adapterPosition, itemView.eventRow.toString())


        }


        val eventrow: TextView = itemView.eventRow
        override fun LongClick(position: Int) {
            println("Inside long click")
            return longEventListener!!.LongClick(position)

        }

        override fun onLongClick(v: View?): Boolean {
            LongClick(adapterPosition)
            return true
        }

        override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenuInfo?) {
            menu!!.setHeaderTitle("Event Options")
            menu.add(this.adapterPosition,121,0,"Edit")
            menu.add(this.adapterPosition,122,1,"Delete")
        }



    }

}



