package com.example.share_the_cost_app.activities


import android.content.Intent

import android.os.Bundle
import android.view.View

import android.widget.Toast

import androidx.recyclerview.widget.LinearLayoutManager
import com.example.share_the_cost_app.Adapters.AttendeeAdapter

import com.example.share_the_cost_app.model.*
import com.example.share_the_cost_app.activities.BaseActivity.OnNoteListener
import com.example.share_the_cost_app.api.Api
import com.example.share_the_cost_app.model.Responses.AttendeeResponse
import kotlinx.android.synthetic.main.activity_add_user.*
import retrofit2.Retrofit
import com.example.share_the_cost_app.storage.SharedPrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.converter.gson.GsonConverterFactory


class AddUser : BaseActivity(), OnNoteListener {

    private var eventAttendList: List<Attendee>? = null
    private val eventAttendees = ArrayList<Attendee>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.share_the_cost_app.R.layout.activity_add_user)

        val actionBar = supportActionBar
        actionBar!!.title = "Add User"

        val prefs = SharedPrefManager.getInstance(applicationContext)
        if (prefs.readAttendees().isEmpty()) {
            val api = retrofit.create(Api::class.java)

            api.allAttendees().enqueue(object : Callback<AttendeeResponse> {

                override fun onFailure(call: Call<AttendeeResponse>, t: Throwable) {
                    println("on Failure JSON not found")
                    println(t.localizedMessage)
                }

                override fun onResponse(
                    call: Call<AttendeeResponse>,
                    response: Response<AttendeeResponse>
                ) {

                    println("response from api: ${response.body()}")
                    prefs.saveAllAttendees(response.body()!!.attendee)

                    val attending = ArrayList<Attendee>()
                    response.body()!!.attendee.forEach { attendee ->
                        if (attendee.name.equals(prefs.user.name)) {
                            attending.add(attendee)
                        }
                    }
                    prefs.saveAttendeeList(attending)
                    prefs.readAttendees()
                    println("Api attend:" + prefs.attending)
                    println("Api not-attend:" + prefs.notAttending)
                    //readAtt()
                }
            })
        } else {
            println("attend:" + prefs.attending)
            println("not-attend:" + prefs.notAttending)

        }

        eventAttendees.addAll(prefs.attending.values.toList())

        showData(prefs.notAttending.values.toList())

        // NEW USER SCREEN TO INSERT INTO DB
        btn_newUser.setOnClickListener() {
            val intent = Intent(applicationContext, NewUser::class.java)

            startActivity(intent)
            //Toast.makeText(this, "new user screen has to be made", Toast.LENGTH_SHORT).show()
        }
        loadNewAttendees()


        //END ONCREATE
    }

    override fun onNoteClick(position: Int, checked: Boolean) {
        //Check if attendeeList is empty and that position is not null
        if (eventAttendList == null || position < 0 || position >= eventAttendList!!.size) {
            return
        }

        val attendee: Attendee = eventAttendList!!.get(position)
        println("----clicked response from activity Id: " + eventAttendList!!.get(position).id)

        checkListener(attendee, checked)

        //end
    }

    fun addToEvent(view: View) {

        if (eventAttendees.size < 2) {
            Toast.makeText(this, "Select at least one User", Toast.LENGTH_SHORT).show()

        } else {
            val pref = SharedPrefManager.getInstance(this)
            pref.saveAttendeeList(eventAttendees)
            pref.readAttendees()

            println("!----append to recyclerview----!")
            val intent = Intent(applicationContext, NewEvent::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

            startActivity(intent)
        }
    }

    // Show attendee list from data base
    fun showData(body: List<Attendee>) {
        eventAttendList = body
        val attendeeAdapter =
            AttendeeAdapter(body)
        attendeeAdapter.setOnClickListener(this)

        recyclerView3.apply {
            layoutManager = LinearLayoutManager(this@AddUser)
            adapter = attendeeAdapter
        }
    }

    //checks to see if a attendee is checked and adds them
    // to an array
    fun checkListener(attendee: Attendee, checked: Boolean) {


        if (checked) {
            println("stored to  arrayList")
            eventAttendees.add(attendee)


        } else {
            eventAttendees.remove(attendee)
            println("!---user is removed---!")
        }
        println("attend array content: " + eventAttendees.size)

    }

    fun loadNewAttendees() {

    }

}
