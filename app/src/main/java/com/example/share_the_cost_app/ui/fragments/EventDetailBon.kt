package com.example.share_the_cost_app.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.share_the_cost_app.Adapters.bonFragAdapter
import com.example.share_the_cost_app.R
import com.example.share_the_cost_app.activities.AddBon
import com.example.share_the_cost_app.activities.BaseActivity.OnEventListener
import com.example.share_the_cost_app.activities.BonDetatilActivity
import kotlinx.android.synthetic.main.fragment_event_detail_bon.*


class EventDetailBon : Fragment(), OnEventListener {
    private lateinit var textView: TextView
    private var viewd: View? = null
    private lateinit var recyclerView: RecyclerView
    private var list = ArrayList<String>()


    init {

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (viewd == null) {
            // Inflate the layout for this fragment
            viewd = inflater.inflate(R.layout.fragment_event_detail_bon, container, false)
            val bonAd = bonFragAdapter(list)
            val btn = viewd!!.findViewById<Button>(R.id.Btn_addBon)
            bonAd.setOnClickListener(this)
            btn.setOnClickListener { addBon() }
            recyclerView = viewd!!.findViewById(R.id.bon_overview)
            recyclerView.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = bonAd
            }
            textView = viewd!!.findViewById(R.id.bon_overal)
            textView.setText(arguments?.getString("message"))
            list.add("Ah Bon")
            list.add("Praxis Bon")


        }


        return viewd


    }


    override fun onEventClick(position: Int, eventD: String) {
        println("clicked into BOn detail frag:   $position")
        if (list == null || position < 0) {
            println("returned null on bon list")
            return
        }
        val bonName = list.get(position)

        val intent = Intent(context, BonDetatilActivity::class.java)
        intent.putExtra("bonName", bonName)
        startActivity(intent)
    }

    fun addBon() {
       val intent=Intent(context,AddBon::class.java)
        startActivity(intent)
    }

}