package com.example.share_the_cost_app.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.recyclerview.widget.RecyclerView
import com.example.share_the_cost_app.R
import kotlinx.android.synthetic.main.attendee_row.view.*
import com.example.share_the_cost_app.activities.BaseActivity.OnNoteListener
import com.example.share_the_cost_app.model.Attendee


class AttendeeAdapter(private val attendee: List<Attendee>) :
    RecyclerView.Adapter<AttendeeAdapter.ViewHolder>() {

    private var mOnNoteListener: OnNoteListener? = null

    fun setOnClickListener(listener: OnNoteListener?) {
        mOnNoteListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.attendee_row, parent, false)
        val viewHolder =
            ViewHolder(
                view
            )
        viewHolder.setOnClickListener(mOnNoteListener)
        return viewHolder
    }

    override fun getItemCount() = attendee.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val Atd = attendee[position]
        attendee.get(position)
        holder.attendeerow.text = Atd.name
        holder.checkAttende.setOnCheckedChangeListener(null)


    }

    class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private var mOnNoteListener: OnNoteListener? = null
        val checkAttende = itemView.checkAtd


        init {
            checkAttende.setOnClickListener(this)


        }

        fun setOnClickListener(listener: OnNoteListener?) {
            mOnNoteListener = listener
        }

        override fun onClick(v: View?) {


            mOnNoteListener?.onNoteClick(adapterPosition, itemView.checkAtd.isChecked)


        }

        val attendeerow: CheckBox = itemView.checkAtd
    }


}


