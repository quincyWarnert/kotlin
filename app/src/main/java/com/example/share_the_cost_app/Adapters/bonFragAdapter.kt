package com.example.share_the_cost_app.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.share_the_cost_app.R

import com.example.share_the_cost_app.activities.BaseActivity.OnEventListener

import kotlinx.android.synthetic.main.eventattendee_row.view.*

class bonFragAdapter(private val bon: ArrayList<String>) :
    RecyclerView.Adapter<bonFragAdapter.MyViewHolder>() {
    private  var mOnNoteListener: OnEventListener?=null
    fun setOnClickListener(listener: OnEventListener) {
        mOnNoteListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):MyViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.eventattendee_row, parent, false)
        val viewHolder = MyViewHolder(view)
        viewHolder.setOnClickListener(mOnNoteListener)
        return viewHolder
    }

    override fun getItemCount(): Int {
        return bon.size
    }

    override fun onBindViewHolder(holder:MyViewHolder, position: Int) {
        val data = bon[position]
        data.get(position)
        holder.bonList.text = data

    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private  var mOnNoteListener:OnEventListener?=null
        val eventattendeerow = itemView.eventAttendeeRow

        init {
            eventattendeerow.setOnClickListener(this)
        }

        fun setOnClickListener(listener: OnEventListener?) {
            mOnNoteListener = listener
        }
        override fun onClick(v: View?) {
            println("inside BON onclick")
            mOnNoteListener?.onEventClick(adapterPosition, itemView.eventAttendeeRow.toString())
        }
        val bonList: TextView = itemView.eventAttendeeRow
    }
}