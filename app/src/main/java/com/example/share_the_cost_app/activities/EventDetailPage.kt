package com.example.share_the_cost_app.activities
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.example.share_the_cost_app.R
import com.example.share_the_cost_app.Adapters.MyViewPagerAdapter


class EventDetailPage :BaseActivity() {
    private var viewPage: ViewPager? = null
    private var myAdapter: MyViewPagerAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_detail_page)
        val actionBar = supportActionBar

        viewPage = findViewById(R.id.pager)
        myAdapter = MyViewPagerAdapter(
            supportFragmentManager
        )
        viewPage?.adapter = myAdapter

        val eventName = intent.getStringExtra(("eventName"))
        val eventId = intent.getStringExtra("eventId")
        actionBar!!.title = eventName





    }
    //Call to event database retrieve attendees linked to that event
   fun getAttendees (){
        /*use event ID as param to find event in db and get attendees that come along with it
        return them as a list
        set list as a global variable
         */
    }

    //Setup adapter voor recyclerview for attendees
    fun showEventAttendee (){

    }
    /*get list as param and pass it to recyclerview adapter
        **make adapter for recyclerview show attendees**

     */

    /*Setup recyclerview for receipt overview
    receipt should be linked to a specific event
    */
    fun showReceipts(){
        /*
        *
        *
        * */
    }


}
