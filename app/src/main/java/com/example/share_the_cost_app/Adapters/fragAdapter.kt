package com.example.share_the_cost_app.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.share_the_cost_app.R
import kotlinx.android.synthetic.main.eventattendee_row.view.*


class fragAdapter(private val myDataset: ArrayList<String>) :
    RecyclerView.Adapter<fragAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.eventattendee_row, parent, false)
        return MyViewHolder(
            view
        )
    }

    override fun getItemCount(): Int {
        return myDataset.size
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = myDataset[position]
        data.get(position)
        holder.eventattendeerow.text = data
    }
    class MyViewHolder( itemView: View) : RecyclerView.ViewHolder(itemView){
        val eventattendeerow:TextView=itemView.eventAttendeeRow
    }

}
