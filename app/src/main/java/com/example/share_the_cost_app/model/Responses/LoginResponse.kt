package com.example.share_the_cost_app.model.Responses

import com.example.share_the_cost_app.model.User

data class LoginResponse (val error:Boolean,val message:String, val user: User)