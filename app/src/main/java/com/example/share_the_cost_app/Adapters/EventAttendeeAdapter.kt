package com.example.share_the_cost_app.Adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.share_the_cost_app.activities.BaseActivity.OnChangeListener
import com.example.share_the_cost_app.activities.BaseActivity.OnDeleteListener
import com.example.share_the_cost_app.model.Attendee
import kotlinx.android.synthetic.main.generic_row.view.*


class EventAttendeeAdapter(private val attendees:MutableList<Attendee>) :
    RecyclerView.Adapter<EventAttendeeAdapter.ViewHolder>() {
    private var deleteListener:OnDeleteListener?=null
    private var changedListener:OnChangeListener?=null

    fun setOnDeleteListener(listener:OnDeleteListener?){
        deleteListener=listener
    }
    fun setOnChangeListener(listener:OnChangeListener?){
        changedListener=listener
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(com.example.share_the_cost_app.R.layout.generic_row, parent, false)
        val viewHolder=ViewHolder(
            view
        )
        viewHolder.setOnDeleteListener(deleteListener)
        viewHolder.setOnChangeListener(changedListener)
        return viewHolder
    }

    override fun getItemCount() = attendees.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val attendee = attendees[position]

        println("${attendee}" +position)
        holder.name.text = attendee.name
        if (attendee.choice >= 2) {
            holder.percentageDropdown.setSelection(attendee.choice - 1)
        } else {
            holder.percentageDropdown.setSelection(0)
        }
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),View.OnClickListener,
        AdapterView.OnItemSelectedListener {
        private var deleteListener:OnDeleteListener?=null
        private var changedListener:OnChangeListener?=null

        val name = itemView.genericRow
        val deletebtn =itemView.btn_delete
        val percentageDropdown = itemView.percentageHeader
        init {
            deletebtn.setOnClickListener(this)

            //create adapter
            ArrayAdapter.createFromResource(
                itemView.context,
                com.example.share_the_cost_app.R.array.percentList,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                percentageDropdown.adapter = adapter
            }
            percentageDropdown.onItemSelectedListener = this
        }
        override fun onClick(v: View?) {
            deleteListener?.OnDelete(adapterPosition)
        }
        fun setOnDeleteListener(listener:OnDeleteListener?){
          deleteListener=listener
        }
        fun setOnChangeListener(listener:OnChangeListener?){
            changedListener=listener
        }
        val eventattendeetrow: TextView = itemView.genericRow

        override fun onNothingSelected(parent: AdapterView<*>?) {

        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            changedListener?.OnChange(adapterPosition, percentageDropdown.getSelectedItemPosition()+1)
        }


    }

}