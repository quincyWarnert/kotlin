package com.example.share_the_cost_app.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.share_the_cost_app.Adapters.EventAttendeeAdapter
import com.example.share_the_cost_app.api.Api
import com.example.share_the_cost_app.model.Attendee
import com.example.share_the_cost_app.model.Responses.AttendeeResponse
import com.example.share_the_cost_app.storage.SharedPrefManager
import kotlinx.android.synthetic.main.activity_new_event.*
import kotlinx.android.synthetic.main.activity_profile.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class NewEvent : BaseActivity.OnDeleteListener, BaseActivity.OnChangeListener, BaseActivity() {

    private var attendeeList: MutableList<Attendee> = ArrayList<Attendee>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.share_the_cost_app.R.layout.activity_new_event)
        //to change title of activity
        val actionBar = supportActionBar
        actionBar!!.title = "new event"



        // On create load logged in user Into attendee list
        val prefs = SharedPrefManager.getInstance(applicationContext)
        if (prefs.readAttendees().isEmpty()) {
            val api = retrofit.create(Api::class.java)

            api.allAttendees().enqueue(object : Callback<AttendeeResponse> {

                override fun onFailure(call: Call<AttendeeResponse>, t: Throwable) {
                    println("on Failure JSON not found")
                    println(t.localizedMessage)
                }

                override fun onResponse(
                    call: Call<AttendeeResponse>,
                    response: Response<AttendeeResponse>
                ) {

                    println("response from api: ${response.body()}")
                    prefs.saveAllAttendees(response.body()!!.attendee)

                    val attending = ArrayList<Attendee>()
                    response.body()!!.attendee.forEach { attendee ->
                        if (attendee.name.equals(prefs.user.name)) {
                            attending.add(attendee)
                        }
                    }
                    prefs.saveAttendeeList(attending)
                    prefs.readAttendees()
                    println("Api attend:" + prefs.attending)
                    println("Api not-attend:" + prefs.notAttending)
                    readAtt()
                }
            })
        } else {
            println("attend:" + prefs.attending)
            println("not-attend:" + prefs.notAttending)

        }
        loggedInUser(prefs.attending.values.toMutableList())
        //navigate to attendee list

        btn_AddUser.setOnClickListener() {
            adduserView()
        }

//End of onCreate
    }



    fun loggedInUser(attendees: MutableList<Attendee>) {
        attendeeList = attendees

        val eaAdapter = EventAttendeeAdapter(attendees)
        eaAdapter.setOnDeleteListener(this)
        eaAdapter.setOnChangeListener(this)
        eventAttendList.apply {
            layoutManager = LinearLayoutManager(this@NewEvent)
            adapter = eaAdapter
        }

    }


    fun createEvent() {
        //add event name and attendees to database
        //call api and store request
    }

    fun adduserView() {
        val intent = Intent(applicationContext, AddUser::class.java)

        startActivity(intent)
    }

    fun readAtt() {
        try {
            val prefs = SharedPrefManager.getInstance(this)
            val attending = prefs.attending.toMutableMap()
            val toBeRemoved : MutableList<Attendee> = ArrayList<Attendee>()

            println(attending)
            attendeeList.forEachIndexed { index, attendee ->
                if (!attending.containsKey(attendee.id)) {
                    toBeRemoved.add(attendee)
                    println("remove "+attendee.name+" from index "+index)
                    eventAttendList.adapter!!.notifyItemRemoved(index)
                } else {
                    attending.remove(attendee.id)
                    println(attendee.name+" stays in the list on index "+index)
                }
            }
            if (!toBeRemoved.isEmpty()) {
                toBeRemoved.forEach { attendee -> attendeeList.remove(attendee) }
            }
            if (!attending.isEmpty()) {
                val insertIndex = attendeeList.size
                println("insert " + attending + " from index " + insertIndex)
                attendeeList.addAll(attending.values)

                eventAttendList.adapter!!.notifyItemRangeInserted(insertIndex, attending.size)
            }
        } catch (e: Exception) {
            println("readAtt:" + e )
        }
    }

    override fun OnDelete(position: Int) {
        val prefrence = SharedPrefManager.getInstance(this)

        val attending = prefrence.attending.values.toMutableList()
        println(" in item remove function  " + attending)
        println("before press"+attending)
        attending.removeAt(position)
        prefrence.saveAttendeeList(attending)
        prefrence.readAttendees()
        readAtt()
        println("after press $attending")

    }

    override fun OnChange(position: Int, choice: Int) {
        val prefrence = SharedPrefManager.getInstance(this)
        val attending = prefrence.attending.values.toMutableList()

        val old = attending[position]
        println ("Change " + old + " on position "+position+" to "+choice)
        attending[position] = Attendee(old.id, old.name, choice)
        println(attending[position])
        prefrence.saveAttendeeList(attending)
        attendeeList[position] = attending[position]
        eventAttendList.adapter!!.notifyItemChanged(position)
    }



}


