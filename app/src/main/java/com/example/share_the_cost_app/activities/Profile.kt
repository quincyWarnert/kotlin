package com.example.share_the_cost_app.activities

import android.content.Intent
import android.os.Bundle

import com.example.share_the_cost_app.storage.SharedPrefManager
import kotlinx.android.synthetic.main.activity_profile.*

import android.view.View


import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner


class Profile : BaseActivity(), AdapterView.OnItemSelectedListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(com.example.share_the_cost_app.R.layout.activity_profile)
        //to change title of activity
        val actionBar = supportActionBar
        actionBar!!.title = "Profile"

        getUserName()

        val spinner: Spinner = findViewById(com.example.share_the_cost_app.R.id.percentageHeader)
        spinner.onItemSelectedListener = this
        //create adapter
        ArrayAdapter.createFromResource(
            this,
            com.example.share_the_cost_app.R.array.percentList,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }

    }


    fun eventOverview(view: View) {
        val OVIntent = Intent(applicationContext, EventOverView::class.java)

        startActivity(OVIntent)

    }

    fun attendees(view: View) {

        val intent = Intent(applicationContext, AddUser::class.java)

        startActivity(intent)

    }


    fun getUserName() {
        val loggedUsername = intent.getStringExtra("username")
        userName.apply { text = loggedUsername }


    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val userChoice = percentageHeader.getSelectedItemPosition()
        val sharedPref = getSharedPreferences("my_shared_preff", 0)
        val prefEditor = sharedPref.edit()
        prefEditor.putInt("choice", userChoice)
        prefEditor.apply()


    }

    override fun onStart() {
        super.onStart()

        if (!SharedPrefManager.getInstance(this).isLoggedIn) {
            val intent = Intent(applicationContext, Login::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
        userName.text = SharedPrefManager.getInstance(applicationContext).user.name.toUpperCase()


        val sharedPref = getSharedPreferences("my_shared_preff", MODE_PRIVATE)
        val spinnerValue = sharedPref.getInt("choice", -1)
        if (spinnerValue != -1) {
            // set the selected value of the spinner
            percentageHeader.setSelection(spinnerValue)
        }
    }


}
