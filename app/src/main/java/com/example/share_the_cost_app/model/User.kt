package com.example.share_the_cost_app.model

data class User(
    val id: Int,
    val name: String,
    val email: String,
    val choice: Int
) {

}