package com.example.share_the_cost_app.Adapters

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.share_the_cost_app.ui.fragments.EventDetail
import com.example.share_the_cost_app.ui.fragments.EventDetailBon
import com.example.share_the_cost_app.ui.fragments.EventDetail_IOU

class MyViewPagerAdapter(manager: FragmentManager) :
    FragmentPagerAdapter(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {


    override fun getItem(position: Int): Fragment {

        val eventDetail =
            EventDetail()
        val eventDetailBon =
            EventDetailBon()

        position + 1
        val bundle = Bundle()


        getPageTitle(position)
        eventDetail.arguments = bundle
        var fragment: Fragment? = null
        when (position) {
            0 -> fragment = eventDetail
            1 -> fragment = eventDetailBon

        }
        return fragment!!


    }

    override fun getPageTitle(position: Int): CharSequence? {
        val pageNr = when {
            position == 0 -> "Attendance"
            position == 1 -> "Bon"

            else -> "error"
        }
        return pageNr
    }

    override fun getCount(): Int {
        return 2
    }

}