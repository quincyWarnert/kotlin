package com.example.share_the_cost_app.activities


import android.os.Bundle
import com.example.share_the_cost_app.R

class NewUser : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_user)
        val actionBar = supportActionBar
        actionBar!!.title = "Create New User"
    }
}
