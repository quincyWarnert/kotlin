package com.example.share_the_cost_app.api
import com.example.share_the_cost_app.model.Responses.AttendeeResponse
import com.example.share_the_cost_app.model.Responses.DefaultResponse
import com.example.share_the_cost_app.model.Responses.EventResponse
import com.example.share_the_cost_app.model.Responses.LoginResponse

import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface Api {
    @FormUrlEncoded
    @POST("createuser")
    fun createUser(@Field("name")name:String,
                   @Field("email")email:String,
                   @Field("password")password:String
                   ): Call<DefaultResponse>
    @FormUrlEncoded
    @POST("userlogin")
    fun userLogin(
        @Field("email")email:String,
        @Field("password")password:String
    ):Call<LoginResponse>


    @GET("allevents")
    fun allEvents():Call<EventResponse>

    @GET("allattendees")
    fun allAttendees():Call<AttendeeResponse>

}