package com.example.share_the_cost_app.storage

import com.example.share_the_cost_app.model.User
import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.example.share_the_cost_app.model.Attendee


class SharedPrefManager private constructor(private val mCtx: Context) {
    var notAttending = emptyMap<Int, Attendee>()
    var attending = emptyMap<Int, Attendee>()
    val isLoggedIn: Boolean
        get() {
            val sharedPreferences =
                mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return sharedPreferences.getInt("id", -1) != -1
        }

    val user: User
        get() {
            val sharedPreferences =
                mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return User(
                sharedPreferences.getInt("id", -1),
                sharedPreferences.getString("name", null)!!,
                sharedPreferences.getString("email", null)!!,
                sharedPreferences.getInt("choice", 0)


            )
        }


    fun saveUser(user: User) {

        val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()

        editor.putInt("id", user.id)
        editor.putString("name", user.name)
        editor.putString("email", user.email)
        editor.putInt("choice", user.choice)
        editor.apply()

    }

    //FIX SAVE TYPE ARRAY LIST
    fun saveAllAttendees(attendee: List<Attendee>) {
        val gson = Gson()
        val json = gson.toJson(attendee)
        val prefs = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = prefs.edit()
        editor.putString("allAttendees", json)
        editor.apply()
    }

    fun saveAttendeeList(attendeeIdList: List<Attendee>) {
        val gson = Gson()
        val json = gson.toJson(attendeeIdList)
        val prefs = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = prefs.edit()
        editor.putString("Set", json)
        editor.apply()
    }


    fun readAttendees():List<Attendee> {
        val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val gson = Gson()
        val json = sharedPreferences.getString("Set", "[]")
        val type = object : TypeToken<List<Attendee>>() {}.type
        val json2 = sharedPreferences.getString("allAttendees", "[]")
        val type2 = object : TypeToken<List<Attendee>>() {}.type

        try {


            val eventAttendees: List<Attendee> = gson.fromJson(json, type)
            val allAttendeeList: List<Attendee> = gson.fromJson(json2, type2)
            val allAttend = allAttendeeList.map { it.id to it }.toMap()
            this.attending = eventAttendees.map { it.id to it}.toMap()
            this.notAttending = allAttend.filterKeys { key -> !this.attending.containsKey(key) }
            return allAttendeeList;

        } catch (e: Exception) {
            //TODO Write exception code
            println("${e.message}")
            e.message
        }
        return emptyList()
    }



    fun clear() {
        val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }

    companion object {
        private val SHARED_PREF_NAME = "my_shared_preff"
        private var mInstance: SharedPrefManager? = null
        @Synchronized
        fun getInstance(mCtx: Context): SharedPrefManager {
            if (mInstance == null) {
                mInstance = SharedPrefManager(mCtx)
            }
            return mInstance as SharedPrefManager
        }
    }
}