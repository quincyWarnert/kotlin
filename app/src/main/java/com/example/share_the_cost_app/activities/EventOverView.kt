package com.example.share_the_cost_app.activities

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import com.example.share_the_cost_app.R
import com.example.share_the_cost_app.api.Api
import com.example.share_the_cost_app.model.Event
import com.example.share_the_cost_app.activities.BaseActivity.OnEventListener
import com.example.share_the_cost_app.activities.BaseActivity.OnLongClicktListener
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.share_the_cost_app.Adapters.EventAdapter
import com.example.share_the_cost_app.model.Responses.EventResponse
import com.example.share_the_cost_app.ui.fragments.EventEditFragment
import kotlinx.android.synthetic.main.activity_event_over_view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.NullPointerException


class EventOverView : BaseActivity(), OnEventListener, OnLongClicktListener {
    private var eventPosList: List<Event>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_over_view)
        //to change title of activity
        val actionBar = supportActionBar
        actionBar!!.title = "Events"


        val api = retrofit.create(Api::class.java)
        api.allEvents().enqueue(object : Callback<EventResponse> {

            override fun onFailure(call: Call<EventResponse>, t: Throwable) {
                println("on Failure JSON not found")
                println(t.localizedMessage)
            }

            override fun onResponse(call: Call<EventResponse>, response: Response<EventResponse>) {

                println("response body events ${response.body()}")
                showData(response.body()!!.events)

            }
        })
        btn_create_event.setOnClickListener() {
            val intent = Intent(applicationContext, NewEvent::class.java)

            startActivity(intent)
        }

        //END ONCREATE
    }

    override fun onEventClick(position: Int, eventD: String) {

        if (eventPosList == null || position < 0) {
            println("!!---I have returned null onClick---!!")
            return
        }
        val eventId = eventPosList!!.get(position).id
        val eventIdName = eventPosList!!.get(position).name
        println("postion # $eventId !!!!!")
        val intent = Intent(applicationContext, EventDetailPage::class.java)
        intent.putExtra("Id", eventId)
        intent.putExtra("eventName", eventIdName)
        startActivity(intent)

    }


    fun showData(body: List<Event>) {
        eventPosList = body
        val eventDetailAdapter =
            EventAdapter(body)
        eventDetailAdapter.setOnClickListener(this)
        eventDetailAdapter.setOnLongClickListener(this)

        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@EventOverView)
            adapter = eventDetailAdapter
        }
    }

    override fun LongClick(position: Int) {
        println("start my menu")

        return
    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            121 -> (eventDetail(item.groupId))
            122 -> (deleteEvent(item.groupId)
                    )
            else -> {
                println("error")
            }
        }

        return super.onContextItemSelected(item)
    }

    fun deleteEvent(position: Int) {
        println("Delete event at:  $position")
        Toast.makeText(
            applicationContext,
            "Event deleted",
            Toast.LENGTH_SHORT
        ).show()
    }
fun eventDetail(position: Int){
    val intent = Intent(applicationContext,EventEdit::class.java )
    val eventId = eventPosList!!.get(position).id
    val eventIdName = eventPosList!!.get(position).name

    intent.putExtra("Id", eventId)
    intent.putExtra("eventName", eventIdName)
    //eventId get attendees attached to this event
    startActivity(intent)

}

}
