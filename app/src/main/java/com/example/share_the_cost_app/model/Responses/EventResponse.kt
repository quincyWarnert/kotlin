package com.example.share_the_cost_app.model.Responses

import com.example.share_the_cost_app.model.Event

data class EventResponse (val error: Boolean, val events: List<Event>)