package com.example.share_the_cost_app.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.share_the_cost_app.storage.SharedPrefManager


class MainEmpty : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val activityIntent: Intent

        // go straight to main if a token is stored
        if (SharedPrefManager.getInstance(this).isLoggedIn) {

            if (intent.flags and Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT != 0) {
                // Activity was brought to front and not created,
                // Thus finishing this will get us to the last viewed activity
                finish()
                return
            }else{
                startActivity( Intent(this, Profile::class.java))
            }
        } else {
            startActivity(Intent(this, Login::class.java))
        }


        finish()
    }
}
